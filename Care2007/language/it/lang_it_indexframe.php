<?php

$LDHome='Home';
$LDPatient='Paziente';
$LDAdmission='Accettazione';
$LDAmbulatory='Day Hospital';
$LDMedocs='Cartelle cliniche';
$LDDoctors='Gestione medici';
$LDNursing='Corsie';
$LDOR='Sala operatoria';
$LDLabs='Laboratori';
$LDRadiology='Radiologia';
$LDPharmacy='Farmacia';
$LDMedDepot='Deposito farmaci';
$LDDirectory='Elenco telefonico';
$LDTechSupport='Supporto tecnico';
$LDEDP='CED';
$LDIntraEmail='Posta intranet';
$LDInterEmail='Posta internet';
$LDSpecials='Parametri';
$LDLogin='Login';
$LDLogout='Logout';
$LDAppointments='Appuntamenti';

$VersionChgTarget='_top';
$LDLanguage='Lingua';
$LDPlsWaitInit='Inizializzazione in corso: si prega di attendere...';
$LDChange='Modifica';
/* 2003-04-27 EL */
$LDBrazilian='Brasiliano';
$LDCzech='Ceco';
$LDEnglish='Inglese';
$LDFrench='Francese';
$LDGerman='Tedesco';
$LDIndonesian='Indonesiano';
$LDItalian='Italiano';
$LDNorwegian='Norvegese';
$LDPolish='Polacco';
$LDPortuguese='Portoghese';
$LDSpanish='Spagnolo';
/* 2003-04-28 El */
$LDDutch='Olandese';
# 2003-08-04 EL
$LDPerson='Persona';
# 2003-09-05 EL
$LDUser='Utente';
?>
