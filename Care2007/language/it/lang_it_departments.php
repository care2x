<?php
$LDHeadlines='Titoli';			
$LDCafeNews='News dalla Mensa';		
$LDManagement='Direzione';
$LDHealthTips='Suggerimenti Medici';
$LDEducation='Formazione';
$LDStudies='Studi';
$LDExhibitions='Mostre';
$LDHeadline='Titolo';
$LDAdmission='Accettazione';
$LDCafeteria='News dalla Mensa';
$LDGeneralSurgery='Chirurgia generale';
$LDEmergencySurgery='Chirurgia di emergenza';
$LDPlasticSurgery='Chirurgia plastica';
$LDEarNoseThroath='ENT';
$LDOpthalmology='Oftalmologia';
$LDPathology='Anatomia Patologica';
$LDObGynecology='Ginecologia';
$LDPhysicalTherapy='Fisioterapia';
$LDInternalMedicine='Medicina interna';
$LDIntermediateCareUnit='Intermediate Care Unit';
$LDIntensiveCareUnit='Terapia intensiva';
$LDEmergencyAmbulatory='Pronto Soccorso';
$LDInternalMedicineAmbulatory='Day Hospital di Medicina Interna';
$LDSonography='Ecografia';
$LDNuclearDiagnostics='Radiodiagnostica';
$LDRadiology='Radiologia';
$LDOncology='Oncologia';
$LDNeonatal='Ostetricia';
$LDCentralLaboratory='Laboratorio clinico';
$LDSerologicalLaboratory='Laboratorio immunologico';
$LDChemicalLaboratory='Laboratorio medico';
$LDBacteriologicalLaboratory='Laboratorio analisi batteriologiche';
$LDTechnicalMaintenance='Manutenzione tecnica';
$LDITDepartment='CED';
$LDGeneralAmbulatory='Ambulatorio';
$LDBloodBank='Banca del sangue';
$LDNursing='Infermeria';
/* 2003-04-27 EL */
$LDMedical='Medico';
$LDSupport='Non medico';
$LDNews='Novit&agrave;';
$LDDepartment='Reparto';
$LDPressRelations='Relazioni con il pubblico';
/* 2003-05-19 EL */
$LDSelectDept='Scegli un reparto';
/*2003-06-15 EL*/
$LDPlsSelectDept='Per favore scegli un reparto';
$LD_AllMedicalDept='____Tutti i reparti medici_____';
$LDClinic='Clinica';

#2003-10-23 EL
$LDPlsNameFormal='Inserire il nome formale';
$LDPlsDeptID='Inserire il codice del reparto';
$LDPlsSelectType='Inserire il tipo';
?>
